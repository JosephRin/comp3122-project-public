import 'package:flutter/widgets.dart';

class ProductModel {
  String productId;
  String productName;
  String productImageUrl;
  String productPrice;
  String productDescription;
  String lastPurchased; 

  ProductModel({
    @required this.productId,
    @required this.productName,
    @required this.productImageUrl,
    this.productDescription = "",
    this.productPrice = "",
    this.lastPurchased = "",
  });
}