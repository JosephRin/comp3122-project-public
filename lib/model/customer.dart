import 'package:flutter/widgets.dart';

enum ServingStatus { Pending, Complete, Serving }

class CustomerModel {
  String customerId;
  String customerName;
  String customerImageUrl;
  String memberId;
  String staffId;
  String lastActivity;
  ServingStatus status;

  CustomerModel({
    @required this.customerId, 
    @required this.customerName, 
    @required this.customerImageUrl,
    @required this.staffId,
    @required this.lastActivity,
    @required statusFlag,
    @required this.memberId,
  }){
    if(statusFlag == "P"){
      this.status = ServingStatus.Pending;
    }else if(statusFlag == "S"){
      this.status = ServingStatus.Serving;
    }else{
      this.status = ServingStatus.Complete;
    }
  }
}