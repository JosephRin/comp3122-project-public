import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:isd/model/customer.dart';

class CustomerCardTheme {
  List<Color> bColor = [Colors.green[400], Colors.green[400], Colors.green[500], Colors.green[600] ];
  List<Color> bActiveColor = [Colors.red[400], Colors.red[400], Colors.red[500], Colors.red[600] ];
  List<Color> bDisableColor = [Colors.grey[400], Colors.grey[400], Colors.grey[500], Colors.grey[600] ];

  TextStyle id = TextStyle(
    color: Colors.white,
    fontSize: 18.0,
    fontWeight: FontWeight.w400,
  );
  
  TextStyle name = TextStyle(
    color: Colors.white,
    fontSize: 28.0,
    fontWeight: FontWeight.w300
  );

  TextStyle nameLg = TextStyle(
    color: Colors.white,
    fontSize: 20.0,
    fontWeight: FontWeight.w300
  );

  TextStyle activity = TextStyle(
    color: Colors.white,
    fontSize: 16.0,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.italic,
  );

  List<Color> getBackgroundColor(ServingStatus status) {
    if(status == ServingStatus.Complete){
      return bDisableColor;
    }else if(status == ServingStatus.Serving){
      return bActiveColor;
    }else{
      return bColor;
    }
  }
}

class RecommendationTheme{
  TextStyle id = TextStyle(
    color: Colors.black,
    fontSize: 20.0,
    fontWeight: FontWeight.w300,
    fontStyle: FontStyle.italic
  );

  TextStyle name = TextStyle(
    color: Colors.black,
    fontSize: 26.0,
    fontWeight: FontWeight.w300,
  );

  TextStyle staffId = TextStyle(
    color: Colors.black,
    fontSize: 18.0,
    fontWeight: FontWeight.w300,
  );

  TextStyle subtitle = TextStyle(
    color: Colors.black,
    fontSize: 18.0,
    fontWeight: FontWeight.w300,
  );

  List<Color> serveColor = [
    Colors.green[400],
    Colors.green[500],
    Colors.green[600],
    Colors.green[600]
  ];

  List<Color> releaseColor = [
    Colors.red[400],
    Colors.red[500],
    Colors.red[600],
    Colors.red[600]
  ];

  List<Color> completeColor = [
    Colors.orange[400],
    Colors.orange[500],
    Colors.orange[600],
    Colors.orange[600]
  ];

  List<Color> disableColor = [
    Colors.grey[400],
    Colors.grey[500],
    Colors.grey[600],
    Colors.grey[600]
  ];
}

class GlobalTheme {
  static CustomerCardTheme customerCardTheme = CustomerCardTheme();
  static RecommendationTheme recommendationTheme = RecommendationTheme();
}