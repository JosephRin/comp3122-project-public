import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SelectField extends StatefulWidget{
  final String title;
  final String hint;
  final Map<String,String> options;
  final TextEditingController textController;
  final bool editable;
  final String value;

  SelectField({@required this.title, this.hint = "", this.options, @required this.textController, this.editable = true, this.value = ""});

  @override
  _SelectFieldState createState() => _SelectFieldState();
}

class _SelectFieldState extends State<SelectField> {
  int _selectedIndex;
  TextEditingController _displayController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if(widget.value != ""){
      _displayController.text = widget.value;
    }
    widget.textController.addListener(() => {
      if(widget.textController.text == ""){
        _displayController.text = ""
      }
    });
  }

  @override
  void dispose() {
    _displayController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12.0),
      child: TextFormField(
        controller: _displayController,
        onTap: (widget.editable) ? () async {
          int index = await showModalBottomSheet<int>(
            context: context,
            builder: (context) => _CupertinoDropDown(_selectedIndex, widget.options),
          );
          if(widget.options.isEmpty) return;
          setState(() {
           _selectedIndex = index; 
          });
          if(_selectedIndex != null){
            String value = widget.options.keys.elementAt(_selectedIndex);
            widget.textController.text = value;
            _displayController.text = widget.options[value];
          }else{
            _displayController.text = widget.options[widget.textController.text];
          }
        } : null,
        readOnly: true,
        validator: (value) => (value.isEmpty) ? "不能留空" : null,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          suffixIcon: Icon(Icons.arrow_drop_down),
          contentPadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
          enabledBorder: (widget.editable) ? OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: BorderSide(color: Colors.blue[500],),
          ) : OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: BorderSide(color: Colors.grey[400],),
          ),
          labelText: this.widget.title,
          labelStyle: TextStyle(
            color: Colors.blue[500],
          ),
          focusedBorder: (widget.editable) ? OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: BorderSide(color: Colors.blue[500], width: 2.0),
          ) : OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            borderSide: BorderSide(color: Colors.grey[400],),
          ),
        ),
      ),
    );
  }
}

class _CupertinoDropDown extends StatefulWidget{
  final int selectedItem;
  final Map<String,String> options;

  _CupertinoDropDown(this.selectedItem, this.options);

  @override
  __CupertinoDropDownState createState() => __CupertinoDropDownState();
}

class __CupertinoDropDownState extends State<_CupertinoDropDown> {
  int _selectedItemIndex;
  FixedExtentScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    setState(() {
     _selectedItemIndex = (widget.selectedItem == null) ? 0 : widget.selectedItem; 
    });
    _scrollController = new FixedExtentScrollController(initialItem: _selectedItemIndex);
  }

  List<Widget> _getOptions(){
    List<Widget> widgets = new List<Widget>();
    for(String option in widget.options.values){
      widgets.add(Text(option));
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).copyWith().size.height/3,
      color: CupertinoColors.lightBackgroundGray,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: CupertinoButton(
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.of(context).pop(null);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: CupertinoButton(
                  child: Text("Confirm"),
                  onPressed: (){
                    Navigator.of(context).pop(_selectedItemIndex);
                  },
                ),
              )
            ],
          ),
          Expanded(
            child: SafeArea(
              child: CupertinoPicker(
                backgroundColor: CupertinoColors.lightBackgroundGray,
                scrollController: _scrollController,
                magnification: 1.0,
                children: _getOptions(),
                itemExtent: 40.0,
                useMagnifier: true,
                onSelectedItemChanged: (int index){
                  setState(() {
                  _selectedItemIndex = index; 
                  });
                },
              ),
            ),
          )
        ],
      )
    );
  }
}