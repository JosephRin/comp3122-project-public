import 'dart:io';
import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:isd/service/session.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:random_string/random_string.dart';

class ImageUploader extends StatefulWidget{
  final String bucket;
  final String title;
  ImageUploader({@required this.bucket, @required this.title});

  @override
  _ImageUploaderState createState() => _ImageUploaderState();
}

class _ImageUploaderState extends State<ImageUploader> {
  String _imageUrl;
  bool _isValid;

  @override
  void initState() {
    _imageUrl = null;
    _isValid = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        (_imageUrl == null) ? Container(
          width: double.infinity,
          height: 230.0,
          padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 5.0),
          child: RaisedButton(
            color: Colors.deepOrange[900],
            shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            padding: EdgeInsets.symmetric(vertical: 12.0),
            child: Text(
              widget.title,
              style: TextStyle(color: Colors.grey[300], fontSize: 42.0),
            ),
            onPressed: () async {
              final imageUrl = await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ImageCaptuer(widget.bucket),
              ));
              setState(() {
                _imageUrl = imageUrl;
              });
            },
          ),
        ) : 
        Container(
          width: double.infinity,
          height: 230.0,
          padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 5.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Padding(
                  padding: EdgeInsets.only(left: 3.0),
                  child: RaisedButton(
                    color: Colors.green,
                    shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(28.0)),
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    child: Text(
                      "OK",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: (){
                      setState(() {
                        _imageUrl = null;
                      });
                    },
                  ),
                )
                
              )
            ],
          ),
        ),
        Visibility(
          child: TextFormField(
            enabled: false,
            validator: (value){
              if(value == ""){
                setState(() {
                  _isValid = false;
                });
                return "";
              }
              setState(() {
                _isValid = true;
              });
              return null;
            },
          ),
          visible: false,
          maintainSize: false,
          maintainInteractivity: false,
          maintainState: true,
        ),
        if(!_isValid) Container(
          padding: EdgeInsets.only(left: 35.0, right: 20.0, bottom: 10.0),
          child: Text(
            "Not yet upload file",
            style: TextStyle(
              fontSize: 12.0,
              color: Colors.red[700],
            ),
          ),
        ),
      ],
    );
  }
}

class FileUploader extends StatefulWidget{
  final File file;
  final String bucket;
  FileUploader({@required this.file, @required this.bucket});
  @override
  _FileUploaderState createState() => _FileUploaderState();
}

class _FileUploaderState extends State<FileUploader> {
  @override
  Widget build(BuildContext context) {
    return FlatButton.icon(
      label: Text("Upload"),
      color: Colors.transparent,
      textColor: Colors.grey[100],
      icon: Icon(Icons.file_upload),
      onPressed: () async {
        ProgressDialog pr = new ProgressDialog(context, isDismissible: false);
        pr.style(
          message: "Uploading...",
          progressWidget: Container(
            padding: EdgeInsets.all(8.0),
            child: CircularProgressIndicator(),
          ),
          progress: 0,
        );
        pr.show();
        String filename = randomAlphaNumeric(10) + ".jpg";
        final uploadedImageUrl = await AmazonS3Cognito.upload(widget.file.path, widget.bucket, "---hidden for safety--", filename, AwsRegion.US_EAST_2, AwsRegion.US_EAST_2);
        if(uploadedImageUrl != "Failed"){
          pr.update(
            message: "Successful",
            progressWidget: Container(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.check),
            )
          );
          await Future.delayed(Duration(seconds: 2,));
          await pr.hide();
          Navigator.pop(context, uploadedImageUrl);
        }else{
          pr.update(
            message: "Failed",
            progressWidget: Container(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.close),
            )
          );
          await Future.delayed(Duration(seconds: 2,));
          await pr.hide();
        }
      },
    );
  }
}

class ImageCaptuer extends StatefulWidget{
  final String bucket;
  ImageCaptuer(this.bucket);
  @override
  _ImageCaptuerState createState() => _ImageCaptuerState();
}

class _ImageCaptuerState extends State<ImageCaptuer> {
  File _imageFile;

  Future<void> _pickImage(ImageSource imageSource) async {
    File selected = await ImagePicker.pickImage(source: imageSource, imageQuality: 70);
    
    setState(() {
      _imageFile = selected;
    });
  }

  Future<void> _cropImage() async {
    File cropped = await ImageCropper.cropImage(
      sourcePath: _imageFile.path,
    );
    
    setState(() {
      _imageFile = cropped ?? _imageFile;
    });
  }

  void _clear(){
    setState(() {
      _imageFile = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Upload image", style: TextStyle(color: Colors.grey[200]),),
        backgroundColor: Colors.grey[800],
      ),
      backgroundColor: Colors.grey[900],
      bottomNavigationBar: BottomAppBar(
        color: Colors.grey[800],
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            if(_imageFile == null) ... [
              Expanded(
                flex: 5,
                child: Container(
                  height: 60.0,
                  child: IconButton(
                    color: Colors.grey[400],
                    icon: Icon(Icons.photo_camera),
                    onPressed: () => _pickImage(ImageSource.camera),
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  height: 60.0,
                  child: IconButton(
                    color: Colors.grey[400],
                    icon: Icon(Icons.photo_library),
                    onPressed: () => _pickImage(ImageSource.gallery),
                  ),
                ),
              ),
            ],
            if(_imageFile != null) ... [
              Expanded(
                child: Container(
                  height: 60.0,
                  child: FileUploader(file: _imageFile, bucket: widget.bucket,),
                ),
              ),
            ]
          ],
        ),
      ),
      body: (_imageFile != null) ? ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(25.0),
            child: Image.file(_imageFile),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton(
                color: Colors.blue[600],
                child: Icon(Icons.crop),
                onPressed: _cropImage,
              ),
              FlatButton(
                color: Colors.red[600],
                child: Icon(FontAwesomeIcons.trash),
                onPressed: _clear,
              ),
            ],
          ),
          SizedBox(height: 20.0,),
        ],
      ) : 
      Center(
        child: Text(
          "Click below buttons to choose file",
          style: TextStyle(
            fontSize: 18.0,
            color: Colors.grey[200],
          ),
        ),
      ),
    );
  }
} 