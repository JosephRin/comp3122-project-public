import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:isd/model/customer.dart';
import 'package:isd/model/global_theme.dart';
import 'package:isd/view/recommendation/recommendation.dart';

class Customer extends StatefulWidget{
  final CustomerModel customerModel;
  Customer(this.customerModel);
  @override
  _CustomerState createState() => _CustomerState();
}

class _CustomerState extends State<Customer> {

  @override
  Widget build(BuildContext context) {
    final thumbnail = Container(
      alignment: new FractionalOffset(0.0, 0.5),
      margin: const EdgeInsets.only(left: 10.0),
      child: Hero(
        tag: widget.customerModel.customerId,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(72.0), 
          child: Image.network(
            widget.customerModel.customerImageUrl,
            width: 110.0,
            height: 110.0,
            fit: BoxFit.cover,
            loadingBuilder:(BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
              if (loadingProgress == null) return child;
              return Container(
                width: 110.0,
                height: 110.0,
                decoration: BoxDecoration(
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.circular(72.0)
                ),
                child: Padding(
                  padding: EdgeInsets.all(35.0),
                  child: CircularProgressIndicator(
                  value: loadingProgress.expectedTotalBytes != null ? 
                        loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                        : null,
                  ),
                )
              );
            },
          ),
        )
      ),
    );

    final card = Container(
      margin: const EdgeInsets.only(left: 62.0, right: 24.0),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: <BoxShadow>[
          BoxShadow(color: Colors.grey[500],
            blurRadius: 10.0,
            offset: new Offset(2.0, 8.0))
        ],
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0, 0.3, 0.6, 0.9],
          colors: GlobalTheme.customerCardTheme.getBackgroundColor(widget.customerModel.status),
        )
      ),
      child: Container(
        margin: EdgeInsets.only(top: 16.0, left: 16.0),
        constraints: BoxConstraints.expand(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left:46.0),
              child: Text(widget.customerModel.memberId, style: GlobalTheme.customerCardTheme.id,),
            ),
            Container(
              color: Colors.white,
              width: 48.0,
              height: 0.8,
              margin: EdgeInsets.only(left:48.0, bottom: 10.0),
            ),
            Container(
              height: 40.0,
              alignment: Alignment.center,
              margin: EdgeInsets.only(left: 20.0, bottom: 12.0),
              child: Text(
                widget.customerModel.customerName, 
                style: (widget.customerModel.customerName.length > 10) ? GlobalTheme.customerCardTheme.nameLg : GlobalTheme.customerCardTheme.name,
                maxLines: 1,
                softWrap: false,
                textScaleFactor: min(1, 20 / widget.customerModel.customerName.length),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.only(right: 24.0),
              child: Text(widget.customerModel.lastActivity, style: GlobalTheme.customerCardTheme.activity,),
            ),
          ],
        ),
      ),
    );
    return Container(
      margin: EdgeInsets.only(top: 16.0, bottom: 8.0),
      child: FlatButton(
        onPressed: () => {
          Navigator.push(
            context, 
            MaterialPageRoute(builder: (context) => Recommendation(widget.customerModel))
          ).then((val) => {
            setState((){})
          })
        },
        child: Stack(
          children: <Widget>[
            card,
            thumbnail,
          ],
        ),
      )
    );
  }
}