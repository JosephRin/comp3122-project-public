import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:isd/model/customer.dart';
import 'package:isd/model/repository.dart';
import 'package:isd/service/api.dart';
import 'package:isd/view/cctv.dart';
import 'package:isd/view/util/select_field.dart';

import 'customer.dart';

class CustomerList extends StatefulWidget{
  @override
  _CustomerListState createState() => _CustomerListState();
}

class _CustomerListState extends State<CustomerList> {
  TextEditingController _textEditingController = new TextEditingController();
  Map<String,String> _staffList = {};

  @override
  void initState() {
    _textEditingController.text = Repository.staffId;
    super.initState();
  }

  @override
  void dispose(){
    _textEditingController.dispose();
    super.dispose();
  }

  Future<void> _onRefresh() async{
    await _getInfo();
    if(!mounted) return;
    setState(() {
      
    });
  }

  Future<List<CustomerModel>> _getInfo() async{
    try{
      List<CustomerModel> info;
      APIService api = new APIService();
      info = [];
      final response = await api.getActiveCustomer();
      for(final customer in response["data"]){
        final imageResponse = await api.getCustomerImage(customer["customerid"]);
        if(imageResponse["status"] != null && imageResponse["status"] == false){
          return [];
        }
        info.add(new CustomerModel(
          staffId: customer["staffid"],
          customerId: customer["customerid"],
          memberId: customer["memberid"],
          customerName: customer["customername"],
          customerImageUrl: imageResponse["data"],
          lastActivity: customer["lastactivity"],
          statusFlag: customer["flag"],
        ));
      }

      /* get staff list */
      Map<String,String> staffs = {};
      final staffResponse = await api.getStaffList();
      for(final staff in staffResponse["data"]){
        staffs.addAll({staff["staffid"]: staff["staffid"]});
      }
      _staffList = staffs;

      return info;
    }catch(e){
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("Customer List"),
            IconButton(
              icon: Icon(FontAwesomeIcons.userCog),
              onPressed: (){
                _textEditingController.text = Repository.staffId;
                AlertDialog alertDialog = AlertDialog(
                  title: Text("Change Staff ID (" + Repository.staffId + ")"),
                  content: SelectField(
                    title: "Staff ID",
                    textController: _textEditingController,
                    options: _staffList,
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("Cancel"),
                      onPressed: () async {
                        Navigator.of(context).pop();
                      },
                    ),
                    FlatButton(
                      child: Text("Confirm"),
                      onPressed: (){
                        Repository.staffId = _textEditingController.text;
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                );
                showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context){
                    return alertDialog;
                  }
                );
              },
            )
          ],
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                Colors.red[400],
                Colors.red[400],
                Colors.red[500],
                Colors.red[600]
              ],
            ),
          ),
        ),
        leading: RaisedButton(
          color: Colors.grey[900],
          child: Icon(
            FontAwesomeIcons.video,
            color: Colors.white,
          ),
          onPressed: (){
            Navigator.push(
              context, 
              MaterialPageRoute(builder: (context) => CCTV())
            );
          }
        ),
      ),
      backgroundColor: Colors.grey[300],
      body: RefreshIndicator(
        onRefresh: () => _onRefresh(),
        child: FutureBuilder(
          future: _getInfo(),
          builder: (context, info){
            if(info.hasData == null || info.data == null){
              return Container(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              );
            }
            final itemList = info.data.reversed.toList();
            return Container(
              child: (itemList.isEmpty) ? Center(
                child: RaisedButton(
                  color: Colors.white,
                  child: Text("Refresh"),
                  onPressed: (){
                    setState(() {
                      
                    });
                  },
                ),
              ) :
              CustomScrollView(
                scrollDirection: Axis.vertical,
                slivers: <Widget>[
                  SliverPadding(
                    padding: const EdgeInsets.symmetric(vertical: 24.0),
                    sliver: SliverFixedExtentList(
                      delegate: SliverChildBuilderDelegate(
                        (context, index) => Customer(itemList[index]),
                        childCount: itemList.length
                      ), itemExtent: 155.0,
                    ),
                  ),
                ],
              )
            );
          },
        ),
      ),
    );
  }
}