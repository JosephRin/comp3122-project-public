import 'dart:math';

import 'package:flutter/material.dart';
import 'package:isd/model/product.dart';

class Product extends StatefulWidget{
  final ProductModel productModel;
  Product({@required this.productModel});
  @override
  _ProductState createState() => _ProductState();
}

class _ProductState extends State<Product> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      padding: EdgeInsets.all(5.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12.0)
      ),
      alignment: Alignment.bottomCenter,
      width: 170.0,
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(12.0),
            child: Image.network(
              widget.productModel.productImageUrl,
              height: 145.0,
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(5.0),  
            height: 25.0,       
            child: Text(
              widget.productModel.productName,
              style: TextStyle(fontSize: 16.0),
              maxLines: 1,
              softWrap: false,
              textScaleFactor: min(1, 16 / widget.productModel.productName.length),
            ),
          ),
        ],
      ),
    );
  }
}