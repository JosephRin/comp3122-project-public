import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:isd/model/customer.dart';
import 'package:isd/model/global_theme.dart';
import 'package:isd/model/product.dart';
import 'package:isd/model/repository.dart';
import 'package:isd/service/api.dart';
import 'package:isd/view/recommendation/product.dart';

class Recommendation extends StatefulWidget{
  final CustomerModel _customerModel;
  Recommendation(this._customerModel);
  @override
  _RecommendationState createState() => _RecommendationState();
}

class _RecommendationState extends State<Recommendation> {
  final APIService _api = new APIService();
  String _servingStaff = "";
  bool _isServing;
  bool _isCompleted;
  CustomerModel _customerModel;
  bool _isThatStaff;
  bool _init = false;

  @override
  initState(){
    super.initState();
    setState(() {
      _customerModel = widget._customerModel;
    });
  }

  Future<bool> _getCustomerInfo() async {
    if(_init) return true;
    try{
      APIService api = new APIService();
      final response = await api.getActiveCustomer(customerId: widget._customerModel.customerId);
      final customer = response["data"][0];
      final imageResponse = await api.getCustomerImage(customer["customerid"]);
      if(imageResponse["status"] != null && imageResponse["status"] == false){
        return false;
      }
      _customerModel = new CustomerModel(
        staffId: customer["staffid"],
        customerId: customer["customerid"],
        memberId: customer["memberid"],
        customerName: customer["customername"],
        customerImageUrl: imageResponse["data"],
        lastActivity: customer["lastactivity"],
        statusFlag: customer["flag"],
      );

      _servingStaff = _customerModel.staffId;
      _isThatStaff = (_servingStaff == Repository.staffId) ? true : false;
      _isServing = (_customerModel.status == ServingStatus.Pending) ? false : true;
      _isCompleted = (_customerModel.status == ServingStatus.Complete) ? true : false;

      _init = true;

      return true;
    }catch(e){
      return false;
    }
  }

  Future<List<ProductModel>> _getPurchasedItem() async {
    if(_customerModel.memberId == "---" || _customerModel.memberId == "M00000"){
      return [];
    }
    try{
      List<ProductModel> items = [];
      final itemResponse = await _api.getPurchasedItem(_customerModel.memberId);
      for(final item in itemResponse["data"]){
        final imageReponse = await _api.getItemImage(item["productid"]);
        items.add(ProductModel(
          productId: item["productid"],
          productName: item["productname"],
          productImageUrl: imageReponse["data"],
        ));
      }
      return items;
    }catch(e){
      return [];
    }
  }

  Future<List<ProductModel>> _getRecommendItem() async {
    try{
      List<ProductModel> items = [];
      final itemResponse = await _api.getRecommendItem(_customerModel.customerId);
      for(final item in itemResponse["data"]){
        final imageReponse = await _api.getItemImage(item["productid"]);
        items.add(ProductModel(
          productId: item["productid"],
          productName: item["productname"],
          productImageUrl: imageReponse["data"],
        ));
      }
      return items;
    }catch(e){
      return [];
    }
  }

  Future<bool> _changeStatus(String status) async {
    try{
      return await _api.changeServingStatus(_customerModel.customerId, Repository.staffId, status);
    }catch(e){
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Recommendation"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: <Color>[
                Colors.orange[400],
                Colors.orange[400],
                Colors.orange[500],
                Colors.orange[600]
              ],
            ),
          ),
        ),
      ),
      body: FutureBuilder(
        future: _getCustomerInfo(),
        builder: (context, info){
          if(info.hasData == null || info.data == null){
            return Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            );
          }
          return SizedBox.expand(
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[
                    Colors.orange[200],
                    Colors.orange[200],
                    Colors.orange[300],
                    Colors.orange[400]
                  ],
                ),
              ),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 150.0,
                      margin: EdgeInsets.all(20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(right: 30.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),
                              child: Image.network(
                                _customerModel.customerImageUrl,
                                width: 150.0,
                                fit: BoxFit.cover,
                                loadingBuilder:(BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                                  if (loadingProgress == null) return child;
                                  return Container(
                                    width: 150.0,
                                    height: 150.0,
                                    child: Padding(
                                      padding: EdgeInsets.all(40.0),
                                      child: CircularProgressIndicator(
                                      value: loadingProgress.expectedTotalBytes != null ? 
                                            loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                                            : null,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 180.0,
                                  margin: EdgeInsets.only(top: 25.0),
                                  child: Text(_customerModel.memberId, style: GlobalTheme.recommendationTheme.id,),
                                ),
                                Container(
                                  color: Colors.black,
                                  width: 48.0,
                                  height: 0.8,
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  width: 180.0,
                                  margin: EdgeInsets.only(top: 20.0),
                                  child: Text(
                                    _customerModel.customerName,
                                    style: GlobalTheme.recommendationTheme.name,
                                    maxLines: 1,
                                    softWrap: false,
                                    textScaleFactor: min(1, 15 / _customerModel.customerName.length),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.bottomLeft,
                                  width: 180.0,
                                  margin: EdgeInsets.only(top: 20.0),
                                  child: Text(
                                    "Being served by: " + _servingStaff, 
                                    style: GlobalTheme.recommendationTheme.staffId,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    ),
                    Container(
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(left: 10.0, bottom: 10.0),
                      height: 20.0,
                      child: Text("Last Purchased:", style: GlobalTheme.recommendationTheme.subtitle,),
                    ),
                    Container(
                      height: 200.0,
                      child: FutureBuilder(
                        future: _getPurchasedItem(),
                        builder: (context, info){
                          if(info.hasData == null || info.data == null){
                            return Container(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(),
                            );
                          }
                          final itemList = info.data.reversed.toList();
                          return Container(
                            height: 200.0,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: <Color>[
                                  Colors.orange[400],
                                  Colors.orange[500],
                                  Colors.orange[600],
                                  Colors.orange[700]
                                ],
                              ),
                              boxShadow: <BoxShadow>[
                                BoxShadow(color: Colors.orange[800],
                                  blurRadius: 5.0,
                                )
                              ]
                            ),
                            child: (itemList.isEmpty) ? Center(child: Text("No record", style: TextStyle(color: Colors.black),),) :
                            ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) => Product(productModel: itemList[index],),
                              itemCount: itemList.length,
                              itemExtent: 200.0,
                            ),
                          );
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomLeft,
                      margin: EdgeInsets.only(left: 10.0, bottom: 10.0),
                      height: 30.0,
                      child: Text("Recommendation:", style: GlobalTheme.recommendationTheme.subtitle,),
                    ),
                    Container(
                      height: 200.0,
                      child: FutureBuilder(
                        future: _getRecommendItem(),
                        builder: (context, info){
                          if(info.hasData == null || info.data == null){
                            return Container(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(),
                            );
                          }
                          final itemList = info.data.reversed.toList();
                          return Container(
                            height: 200.0,
                            width: double.infinity,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: <Color>[
                                  Colors.orange[400],
                                  Colors.orange[500],
                                  Colors.orange[600],
                                  Colors.orange[700]
                                ],
                              ),
                              boxShadow: <BoxShadow>[
                                BoxShadow(color: Colors.orange[800],
                                  blurRadius: 5.0,
                                )
                              ]
                            ),
                            child: (itemList.isEmpty) ? Center(child: Text("No record", style: TextStyle(color: Colors.black),),) :
                            ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) => Product(productModel: itemList[index],),
                              itemCount: itemList.length,
                            ),
                          );
                        },
                      ),
                    ),
                    Container(
                      height: 60.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            height: 40,
                            width: 150.0,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: (_isServing && !_isThatStaff) ? GlobalTheme.recommendationTheme.disableColor : (!_isCompleted) ? ((!_isServing) ? GlobalTheme.recommendationTheme.serveColor : GlobalTheme.recommendationTheme.releaseColor) : GlobalTheme.recommendationTheme.disableColor,
                              ),
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                side: BorderSide(width: 0.5, color: Colors.white),
                                borderRadius: BorderRadius.circular(18.0)
                              ),
                              child: Text(
                                (!_isCompleted) ? ((!_isServing) ? "Serve" : "Release") : "Serve", 
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: (_isServing && !_isThatStaff) ? null : (_isCompleted) ? null : () async {
                                final result = await _changeStatus((_isServing) ? "P" : "S");
                                if(result){
                                  setState(() => {
                                    _servingStaff = (!_isServing) ? Repository.staffId : "---",
                                    _isServing = !_isServing,
                                    _isThatStaff = (_servingStaff == Repository.staffId) ? true : false,
                                  });
                                }else{
                                  // print("error");
                                }
                              },
                            ),
                          ),
                          Container(
                            height: 40,
                            width: 150.0,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: (_isServing && !_isThatStaff) ? GlobalTheme.recommendationTheme.disableColor : (_isServing && !_isCompleted) ? GlobalTheme.recommendationTheme.completeColor: GlobalTheme.recommendationTheme.disableColor,
                              ),
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            child: FlatButton(
                              shape: RoundedRectangleBorder(
                                side: BorderSide(width: 0.5, color: Colors.white),
                                borderRadius: BorderRadius.circular(18.0)
                              ),
                              child: Text(
                                "Complete", 
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: (_isServing && !_isThatStaff) ? null : (_isServing && !_isCompleted) ? () async {
                                final result = await _changeStatus("C");
                                if(result){
                                  setState(() => {
                                    _isThatStaff = false,
                                    _isServing = false,
                                    _isCompleted = true,
                                    _servingStaff = "---",
                                  });
                                }else{
                                  // print("error");
                                }
                              } : null,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      ),
    );
  }
}