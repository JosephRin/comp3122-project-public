
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:isd/view/util/file_upload.dart';

class CCTV extends StatefulWidget{
  @override
  _CCTVState createState() => _CCTVState();
}

class _CCTVState extends State<CCTV> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[900],
        title: Text("Smart CCTV"),
      ),
      backgroundColor: Colors.grey[800],
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ImageUploader(bucket: "create-customers", title: "Add Customer",),
              ],
            )
          ),
          Expanded(
            flex: 1,
            child: Divider(
              thickness: 1.5,
              color: Colors.white,
            ),
          ),
          Expanded(
            flex: 6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ImageUploader(bucket: "delete-customers", title: " Remove Customer",),
              ],
            )
          ),
        ],
      ),
    );
  }
}