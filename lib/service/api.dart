import 'package:isd/service/session.dart';

class APIService {
  final host = "https://5udm974kg6.execute-api.us-east-2.amazonaws.com/SIT";

  Future<Map<String, dynamic>> getActiveCustomer({customerId=""}) async {
    try{
      if(customerId == ""){
        final response = await SessionService.get(host + "/active");
        return response;
      }
      final response = await SessionService.get(host + "/active?customerid=" + customerId);
      return response;
    }catch(e){
      return {"status": false};
    }
  }

  Future<Map<String, dynamic>> getStaffList() async {
    try{
      final response = await SessionService.get(host + "/access");
      return response;
    }catch(e){
      return {"status": false};
    }
  }

  Future<Map<String,dynamic>> getCustomerImage(customerId) async {
    try{
      final response = await SessionService.get(host + "/active/image?customerid=" + customerId);
      return response;
    }catch(e){
      return {"status": false};
    }
  }

  Future<Map<String,dynamic>> getPurchasedItem(memberId) async {
    try{
      final response = await SessionService.get(host + "/member/transaction?memberid=" + memberId);
      return response;
    }catch(e){
      return {"status": false};
    }
  }

  Future<Map<String,dynamic>> getRecommendItem(customerId) async {
    try{
      final response = await SessionService.get(host + "/active/recommendations?customerid=" + customerId);
      return response;
    }catch(e){
      return {"status": false};
    }
  }

  Future<Map<String,dynamic>> getItemImage(productId) async {
    try{
      final response = await SessionService.get(host + "/product/image?productid=" + productId);
      return response;
    }catch(e){
      return {"status": false};
    }
  }

  Future<bool> changeServingStatus(String customerId, String staffId, String status) async {
    try{
      final response = await SessionService.put(host + "/active/staff" + 
        "?customerid=" + customerId + 
        "&staffid=" + staffId + 
        "&status=" + status
      );
      return response;
    }catch(e){
      return false;
    }
  }
}