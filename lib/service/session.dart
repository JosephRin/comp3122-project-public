import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;

class SessionService {
  static Map<String,String> _headers = {};

  static Map<String, String> getHeaders(){
    return _headers;
  }

  static void clearHeader(){
    _headers = {};
  }

  static Future<Uint8List> getImage(String url) async {
    http.Response response = await http.get(url);
    return response.bodyBytes;
  }

  static Future<Map<String,dynamic>> get(String url) async {
    http.Response response = await http.get(url, headers: _headers);
    return json.decode(response.body);
  }

  static Future<bool> put(String url) async {
    try{
      http.Response response = await http.put(url);
      if(response.statusCode == 204){
        return true;
      }else{
        return false;
      }
    }catch(e){
      return false;
    }
  }


}