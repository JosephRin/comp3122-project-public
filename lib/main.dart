import 'package:flutter/material.dart';
import 'package:isd/model/repository.dart';
import 'package:isd/service/api.dart';
import 'package:isd/view/customer/customer_list.dart';
import 'package:isd/view/util/select_field.dart';
import 'package:splashscreen/splashscreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Splash(),
    );
  }

}

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  TextEditingController _textEditingController = new TextEditingController();

  Future<void> loginStaff(context) async {
    APIService api = new APIService();
    /* get staff list */
    Map<String,String> staffs = {};
    final staffResponse = await api.getStaffList();
    for(final staff in staffResponse["data"]){
      staffs.addAll({staff["staffid"]: staff["staffid"]});
    }


    _textEditingController.text = Repository.staffId;
    AlertDialog alertDialog = AlertDialog(
      title: Text("Choose your Staff ID ( Default " + Repository.staffId + ")"),
      content: SelectField(
        title: "Staff ID",
        textController: _textEditingController,
        options: staffs,
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancel"),
          onPressed: () async {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text("Confirm"),
          onPressed: (){
            Repository.staffId = _textEditingController.text;
            Navigator.of(context).pop();
          },
        )
      ],
    );
    await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context){
        return alertDialog;
      }
    );
    return CustomerList();
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 1,
      navigateAfterFuture: loginStaff(context),
      title: new Text('Welcome to ISD Project',
      style: new TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 28.0,
        color: Colors.white
      ),),
      loadingText: Text("the system is initializing...", 
      style: TextStyle(
        color: Colors.white,
        fontStyle: FontStyle.italic,
        ),
      ),
      gradientBackground: LinearGradient(
        colors: [
          Colors.purple[200],
          Colors.purple[200],
          Colors.purple[300],
          Colors.purple[400],
        ],
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
      ),
      styleTextUnderTheLoader: new TextStyle(),
      loaderColor: Colors.white
    );
  }
}
